@IsTest
public class Flow_CreateNewWorkOrderTest {

    private static String returnFault() {
    String fault = 
    'Client ID 12345 Unit Number MA1000\n' + 
    'Clinet Category DHS Client Status ONLINE\n' + 
    'Client Name Mr John Citizen Client Contact Name Mrs Jenny Citizen\n' + 
    'Client Phone 0300000000 (MOB: 0400000000) Client Contact Phone 0311111111\n' + 
    'Client Contact Mobile (MOB: 0411111111)\n' + 
    'Contact Relationship Friend/ neighbour - female\n' + 
    'Client Address 10 Main Street - Some Town VIC 3000\n' + 
    'Map Ref. East West Lane\n' + 
    'Fault Assignment Date 15/01/2021\n' + 
    'Fault Type VISIT\n' + 
    'Priority High\n' + 
    'Notes History: <15/01/2021 17:00:00 AM Help Desk>\n' + 
    'EMAIL TO BE SENT TO THE TECHNICIAN.\n' + 
    '<15/01/2021 16:00:00 AM Client Services>\n' + 
    'Client phoned back to advise of an issue.\n';
    return fault;
    }

    private static String returnNonFault() {
    String nonFault = 
    'Client ID 123456 Dispatch Date 15/01/2021\n' + 
    'Client Name Mr John Citizen Phone no. 0300000000 (MOB: 0400000000)\n' + 
    'Client Address 10 Main Street - Some Town VIC 3000\n' + 
    'Instruction: DHS client doing dcs\n' +
    'Call client for install';
    return nonFault;
    }

    // ************************************
    // ***** createNewWorkOrder Tests *****
    // ************************************
    @IsTest
    static void isCorrectNonFault() {
        
        // Setup
        List<String> flowInput = new List<String>();
        String workOrder = returnNonFault();
        flowInput.add(workOrder);

        // Act
        List<WorkOrder> flowResult = Flow_CreateNewWorkOrder.createNewWorkOrder(flowInput);

        // Assert
        System.assertEquals('0011e00000C8btOAAR', flowResult[0].AccountId);
        System.assertEquals('Dispatched',         flowResult[0].Status);
        System.assertEquals('15/1/2021',          flowResult[0].Dispatched__c.format());
        System.assertEquals('DHS',                flowResult[0].Priority);
        System.assertEquals('INSTALLATION',       flowResult[0].Type__c);
        System.assertEquals(false,                flowResult[0].Key_Lock_Exists__c);
        System.assertEquals(null,                 flowResult[0].Old_Alarm__c);
        System.assertEquals(null,                 flowResult[0].Pricebook2Id);
        System.assertEquals('Work Order Description:\nDHS client doing dcs\nCall client for install',     
                                                  flowResult[0].Work_Order_Description__c);
    }

    @IsTest
    static void isCorrectFault() {
        
        // Setup
        List<String> flowInput = new List<String>();
        String workOrder = returnFault();
        flowInput.add(workOrder);
        String description =     'Notes History:\n' + 
                                 '<15/01/2021 17:00:00 AM Help Desk>\n' + 
                                 'EMAIL TO BE SENT TO THE TECHNICIAN.\n' + 
                                 '<15/01/2021 16:00:00 AM Client Services>\n' + 
                                 'Client phoned back to advise of an issue.';

        // Act
        List<WorkOrder> flowResult = Flow_CreateNewWorkOrder.createNewWorkOrder(flowInput);

        // Assert
        System.assertEquals('0011e00000C8btOAAR', flowResult[0].AccountId);
        System.assertEquals('Dispatched',         flowResult[0].Status);
        System.assertEquals('15/1/2021',          flowResult[0].Dispatched__c.format());
        System.assertEquals('Standard Fault',     flowResult[0].Priority);
        System.assertEquals('FAULT',              flowResult[0].Type__c);
        System.assertEquals(false,                flowResult[0].Key_Lock_Exists__c);
        System.assertEquals('MA1000',             flowResult[0].Old_Alarm__c);
        System.assertEquals(null,                 flowResult[0].Pricebook2Id);
        System.assertEquals(description,          flowResult[0].Work_Order_Description__c);
        System.assertEquals(true,                 flowResult[0].Work_Order_Description__c.startsWith('Notes History:'));
    }
}