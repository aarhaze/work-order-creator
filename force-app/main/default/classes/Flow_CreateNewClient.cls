public with sharing class Flow_CreateNewClient {

    @InvocableMethod(
        label='Create A New MePACS Client'
        description='Returns a new contact record containing the new client information.'
        category='MePACS'
    ) 
    public static List<Contact> createNewClient(List<String> flowInput) {
        Map<String, String> flowLines   = new Map<String, String>();
        List<Contact> clientResult      = new List<Contact>();
        List<String>  flowInputStrings  = flowInput[0].split('\n');

        // Assign inputed work order type
        if (flowInputStrings[0].contains('Unit') && flowInputStrings[1].contains('Clinet')) {
            flowLines.put('Type', 'Fault');
        } else {
            flowLines.put('Type', 'Non-Fault');
        }

        // Assign line items
        for (String str : flowInputStrings) {
            if (str.startsWith('Client ID')) {
                flowLines.put('ClientId', str);
            }
            if (str.startsWith('Client Name')) {
                flowLines.put('Name', str);
            }
            if (str.contains('Phone')) {
                flowLines.put('Phone', str);
            }
            if (str.startsWith('Client Address')) {
                flowLines.put('Address', str);
            }
            if (str.startsWith('Map Ref.')) {
                flowLines.put('Reference', str);
            }
        }  

        // Create string variables
        String clientId = ClientHelper.returnClientId(flowLines.get('ClientId'));
        String ref      = ClientHelper.returnLocationRef(flowLines.get('Reference'));

        // Create map variables
        Map<String, String> name    = ClientHelper.returnNames(flowLines.get('Name'));
        Map<String, String> phone   = ClientHelper.returnPhoneNumbers(flowLines.get('Phone'));
        Map<String, String> address = ClientHelper.returnAddress(flowLines.get('Address'));

        // Create string variables from maps
        String firstName   = name.get('FirstName');
        String lastName    = name.get('LastName');
        String homePhone   = phone.get('HomePhone');
        String mobilePhone = phone.get('MobilePhone');
        String street      = address.get('Street');
        String city        = address.get('City');
        String postcode    = address.get('Postcode');

        // Create new contact
        Contact newContact = new Contact(
            //AccountId         = '0011e00000C8btOAAR',
            Client_ID__c      = clientId,
            FirstName         = firstName,
            LastName          = lastName,
            HomePhone         = homePhone,
            MobilePhone       = mobilePhone,
            MailingStreet     = street,
            MailingCity       = city,
            MailingState      = 'VIC',
            MailingPostalCode = postcode,
            MailingCountry    = 'Australia',
            Location_Reference__c = ref,
            Total_Distance__c = 0
        );

        clientResult.add(newContact);
        return clientResult;
    }
}
