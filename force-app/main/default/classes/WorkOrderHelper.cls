public with sharing class WorkOrderHelper {

    public static Date returnDispatchedDate(String inputStr) {
        if (inputStr == null) {
            return null;
        } Date result = null;
        
        inputStr = inputStr.trim();
        inputStr = inputStr.right(10);
        List<String> dateStrings = inputStr.split('/');

        if (dateStrings.size() == 3 && dateStrings.get(2).length() == 4) {
            result = Date.newInstance(
            Integer.valueOf(dateStrings[2]),
            Integer.valueOf(dateStrings[1]),
            Integer.valueOf(dateStrings[0]));
        } 
        return result;
    }

    public static String returnAlarmUnit(String inputStr) {
        if (inputStr == null) {
            return null;
        } String result = null;

        inputStr = inputStr.trim();
        String alarmUnit = inputStr.right(6);
        if (alarmUnit.isAlphanumeric() && !alarmUnit.containsWhitespace()) {
            result = alarmUnit;
        }
        return result;
    }

    public static String returnCategoryFault(String inputStr) {
        if (inputStr == null) {
            return null;
        } String result = null;

        inputStr = inputStr.trim();
        if (inputStr.containsIgnoreCase('DHS')) {
            result = 'DHS';
        }
        if (inputStr.containsIgnoreCase('Private')) {
            result = 'Private';
        }
        return result;
    }

    public static String returnFaultPriority(String inputStr) {
        if (inputStr == null) {
            return null;
        } String result = 'Standard Fault';

        inputStr = inputStr.trim();
        if (inputStr.containsIgnoreCase('AA') || 
            inputStr.containsIgnoreCase('URGENT') ||
            inputStr.containsIgnoreCase('Very')) {
            result = 'Priority Fault';
        } 
        return result;
    }

    public static String returnNonFaultPriority(List<String> inputStrs) {
        if (inputStrs.size() == 0) {
            return null;
        } String result = 'DHS';

        for (String str : inputStrs) {
            str = str.trim();
            if (str.containsIgnoreCase('Private')) {
                result = 'Private';
                break;
            }
        }
        return result;
    }

    public static String returnType(List<String> inputStrs) {
        if (inputStrs.size() == 0) {
            return null;
        } String result = 'INSTALLATION';

        for (String str : inputStrs) {
            str = str.trim();
            if (str.containsIgnoreCase('re install') ||
                str.containsIgnoreCase('reinstall')  ||
                str.containsIgnoreCase('reinstallation') ||
                str.containsIgnoreCase('re installation')) {
                result = 'REINSTALLATION';
                break;
            }
            if (str.containsIgnoreCase('changeover') ||
                str.containsIgnoreCase('change over')) {
                result = 'REINSTALLATION';
                break;
            }
        }
        return result;
    }

    public static Boolean keyLockRequired(List<String> inputStrs) {
        if (inputStrs.size() == 0) {
            return null;
        } Boolean result = false;

        for (String str : inputStrs) {
            str = str.trim();
            if (str.containsIgnoreCase('key')) {
                result = true;
                break;
            }
        }
        return result;
    }
}
