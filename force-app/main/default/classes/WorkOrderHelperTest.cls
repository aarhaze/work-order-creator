@IsTest
public class WorkOrderHelperTest {

    // ************************************
    // ***** returnDispatchedDate Tests ***
    // ************************************

    @IsTest
    static void checkDateCorrect() {
        
        // Setup
        String inputStr1 = 'Fault Assignment Date 15/01/2021';
        String inputStr2 = 'Client ID 123456 Dispatch Date 15/01/2021';

        // Act
        Date result1 = WorkOrderHelper.returnDispatchedDate(inputStr1);
        Date result2 = WorkOrderHelper.returnDispatchedDate(inputStr2);

        // Assert
        System.assertEquals('15/1/2021', result1.format());
        System.assertEquals('15/1/2021', result2.format());
    }

    @IsTest
    static void checkDateNull() {
        
        // Setup
        String inputStr1 = 'Fault Assignment Date 15/12021';
        String inputStr2 = 'Client ID 123456 Dispatch Date 15/01/21';
        String inputStr3 =  null;

        // Act
        Date result1 = WorkOrderHelper.returnDispatchedDate(inputStr1);
        Date result2 = WorkOrderHelper.returnDispatchedDate(inputStr2);
        Date result3 = WorkOrderHelper.returnDispatchedDate(inputStr3);

        // Assert
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
        System.assertEquals(null, result3);
    }    

    // ************************************
    // ***** returnAlarmUnit Tests ********
    // ************************************

    @IsTest
    static void checkAlarmUnitCorrect() {
        
        // Setup
        String inputStr = 'Client ID 12345 Unit Number MA1000';

        // Act
        String result = WorkOrderHelper.returnAlarmUnit(inputStr);

        // Assert
        System.assertEquals('MA1000', result);
    } 

    @IsTest
    static void checkAlarmUnitCorrectNull() {
        
        // Setup
        String inputStr1 = null;
        String inputStr2 = 'Client ID 12345 Unit Number MA-1000';

        // Act
        String result1 = WorkOrderHelper.returnAlarmUnit(inputStr1);
        String result2 = WorkOrderHelper.returnAlarmUnit(inputStr2);

        // Assert
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
    } 

    // ************************************
    // ***** returnCategoryFault Tests ****
    // ************************************

    @IsTest
    static void checkCategoryCorrect() {
        
        // Setup
        String inputStr1 = 'Clinet Category DHS Client Status ONLINE';
        String inputStr2 = 'Clinet Category Private-HCP Client Status ONLINE';

        // Act
        String result1 = WorkOrderHelper.returnCategoryFault(inputStr1);
        String result2 = WorkOrderHelper.returnCategoryFault(inputStr2);

        // Assert
        System.assertEquals('DHS',     result1);
        System.assertEquals('Private', result2);
    } 

    @IsTest
    static void checkCategoryNull() {
        
        // Setup
        String inputStr1 =  null;
        String inputStr2 = 'Clinet Category Client Status ONLINE';

        // Act
        String result1 = WorkOrderHelper.returnCategoryFault(inputStr1);
        String result2 = WorkOrderHelper.returnCategoryFault(inputStr2);

        // Assert
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
    }

    // ************************************
    // ***** returnPriorityFault Tests ****
    // ************************************

    @IsTest
    static void checkFaultPriorityCorrect() {
        
        // Setup
        String inputStr1 = 'Fault Type VISIT AA URGENT AA Priority Very High';
        String inputStr2 = 'Fault Type VISIT Priority High';
        String inputStr3 =  null;

        // Act
        String result1 = WorkOrderHelper.returnFaultPriority(inputStr1);
        String result2 = WorkOrderHelper.returnFaultPriority(inputStr2);
        String result3 = WorkOrderHelper.returnFaultPriority(inputStr3);

        // Assert
        System.assertEquals('Priority Fault', result1);
        System.assertEquals('Standard Fault', result2);
        System.assertEquals(null, result3);
    } 

    // ************************************
    // ***** returnNonFaultPriority Tests *
    // ************************************

    @IsTest
    static void checkWorkOrderPriorityCorrect() {
        
        // Setup
        List<String> dhsList    = new List<String>{'Instruction: dhs client doing dcs', 'Call client for install'};
        List<String> pteList    = new List<String>{'Instruction: private client doing dcs', 'Call client for install'};
        List<String> returnNull = new List<String>();

        // Act
        String result1 = WorkOrderHelper.returnNonFaultPriority(dhsList);
        String result2 = WorkOrderHelper.returnNonFaultPriority(pteList);
        String result3 = WorkOrderHelper.returnNonFaultPriority(returnNull);

        // Assert
        System.assertEquals('DHS',     result1);
        System.assertEquals('Private', result2);
        System.assertEquals( null,     result3);
    } 

    // ************************************
    // ***** returnType Tests *************
    // ************************************

    @IsTest
    static void returnTypeReinstall() {
        
        // Setup
        List<String> inputStr1 = new List<String>{'Instruction: dhs client doing dcs', 'Call client re install'};
        List<String> inputStr2 = new List<String>{'Instruction: dhs client doing dcs', 'Call client for reinstall'};
        List<String> inputStr3 = new List<String>{'Instruction: dhs client doing dcs', 'Call client reinstallation'};
        List<String> inputStr4 = new List<String>{'Instruction: dhs client doing dcs', 'Call client re installation'};
        List<String> inputStr5 = new List<String>{'Instruction: dhs client doing dcs', 'Call client'};

        // Act
        String result1 = WorkOrderHelper.returnType(inputStr1);
        String result2 = WorkOrderHelper.returnType(inputStr2);
        String result3 = WorkOrderHelper.returnType(inputStr3);
        String result4 = WorkOrderHelper.returnType(inputStr4);
        String result5 = WorkOrderHelper.returnType(inputStr5);

        // Assert
        System.assertEquals('REINSTALLATION', result1);
        System.assertEquals('REINSTALLATION', result2);
        System.assertEquals('REINSTALLATION', result3);
        System.assertEquals('REINSTALLATION', result4);
        System.assertEquals('INSTALLATION',   result5);
    } 

    @IsTest
    static void returnTypeChangeover() {
        
        // Setup
        List<String> inputStr1 = new List<String>{'Instruction: dhs client doing dcs', 'Call client re changeover'};
        List<String> inputStr2 = new List<String>{'Instruction: dhs client doing dcs', 'Call client for change over'};
        List<String> inputStr3 = new List<String>{'Instruction: dhs client doing dcs', 'Call client'};

        // Act
        String result1 = WorkOrderHelper.returnType(inputStr1);
        String result2 = WorkOrderHelper.returnType(inputStr2);
        String result3 = WorkOrderHelper.returnType(inputStr3);


        // Assert
        System.assertEquals('REINSTALLATION', result1);
        System.assertEquals('REINSTALLATION', result2);
        System.assertEquals('INSTALLATION',   result3);
    } 

    // ************************************
    // ***** keyLockRequired Tests ********
    // ************************************

    @IsTest
    static void checkKeyLockRequired() {
        
        // Setup
        List<String> inputStr1 = new List<String>{'Instruction: dhs client', 'Please install keylock', 'Call client'};
        List<String> inputStr2 = new List<String>{'Instruction: dhs client doing dcs', 'Call client for change over'};
        List<String> inputStr3 = new List<String>();

        // Act
        Boolean result1 = WorkOrderHelper.keyLockRequired(inputStr1);
        Boolean result2 = WorkOrderHelper.keyLockRequired(inputStr2);
        Boolean result3 = WorkOrderHelper.keyLockRequired(inputStr3);


        // Assert
        System.assertEquals(true,  result1);
        System.assertEquals(false, result2);
        System.assertEquals(null,  result3);
    } 
}