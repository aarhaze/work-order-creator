@IsTest
public class Flow_CreateNewClientTest {

    private static String returnFault() {
    String fault = 
    'Client ID 12345 Unit Number MA1000\n' + 
    'Clinet Category DHS Client Status ONLINE\n' + 
    'Client Name Mr John Citizen Client Contact Name Mrs Jenny Citizen\n' + 
    'Client Phone 0300000000 (MOB: 0400000000) Client Contact Phone 0311111111\n' + 
    'Client Contact Mobile (MOB: 0411111111)\n' + 
    'Contact Relationship Friend/ neighbour - female\n' + 
    'Client Address 10 Main Street - Some Town VIC 3000\n' + 
    'Map Ref. East West Lane\n' + 
    'Fault Assignment Date 15/01/2021\n' + 
    'Fault Type VISIT\n' + 
    'Priority High\n' + 
    'Notes History: <15/01/2021 17:00:00 AM Help Desk>\n' + 
    'EMAIL TO BE SENT TO THE TECHNICIAN.\n' + 
    '<15/01/2021 16:00:00 AM Client Services>\n' + 
    'Client phoned back to advise of an issue.\n';
    return fault;
    }

    private static String returnNonFault() {
    String nonFault = 
    'Client ID 123456 Dispatch Date 15/01/2021\n' + 
    'Client Name Mr John Citizen Phone no. 0300000000 (MOB: 0400000000)\n' + 
    'Client Address 10 Main Street - Some Town VIC 3000\n' + 
    'Instruction: DHS client doing dcs\n' +
    'Call client for install';
    return nonFault;
    }

    // ************************************
    // ***** createNewClient Tests *******
    // ************************************
    @IsTest
    static void isCorrectNonFault() {
        
        // Setup
        List<String>  flowInput  = new List<String>();
        List<Contact> flowResult = new List<Contact>();
        String workOrder = returnNonFault();
        flowInput.add(workOrder);

        // Act
        flowResult = Flow_CreateNewClient.createNewClient(flowInput);

        // Assert
        System.assertEquals('123456',         flowResult[0].Client_ID__c);
        System.assertEquals('John',           flowResult[0].FirstName);
        System.assertEquals('Citizen',        flowResult[0].LastName);
        System.assertEquals('0300000000',     flowResult[0].HomePhone);
        System.assertEquals('0400000000',     flowResult[0].MobilePhone);
        System.assertEquals('10 Main Street', flowResult[0].MailingStreet);
        System.assertEquals('Some Town',      flowResult[0].MailingCity);
        System.assertEquals('3000',           flowResult[0].MailingPostalCode);
        System.assertEquals(null,             flowResult[0].Location_Reference__c);
    }

    @IsTest
    static void isCorrectFault() {
        
        // Setup
        List<String>  flowInput  = new List<String>();
        List<Contact> flowResult = new List<Contact>();
        String workOrder = returnFault();
        flowInput.add(workOrder);

        // Act
        flowResult = Flow_CreateNewClient.createNewClient(flowInput);

        // Assert
        System.assertEquals('12345',          flowResult[0].Client_ID__c);
        System.assertEquals('John',           flowResult[0].FirstName);
        System.assertEquals('Citizen',        flowResult[0].LastName);
        System.assertEquals('0300000000',     flowResult[0].HomePhone);
        System.assertEquals('0400000000',     flowResult[0].MobilePhone);
        System.assertEquals('10 Main Street', flowResult[0].MailingStreet);
        System.assertEquals('Some Town',      flowResult[0].MailingCity);
        System.assertEquals('3000',           flowResult[0].MailingPostalCode);
        System.assertEquals('East West Lane', flowResult[0].Location_Reference__c);
    }
}
