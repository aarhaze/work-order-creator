public with sharing class Flow_CreateNewWorkOrder {
    @InvocableMethod(
        label='Create A New MePACS Work Order'
        description='Returns a new Work Order record containing the flow input information.'
        category='MePACS'
    ) 
    public static List<WorkOrder> createNewWorkOrder(List<String> flowInput) {
        Map<String, String> flowLines   = new Map<String, String>();
        List<WorkOrder> workOrderResult = new List<WorkOrder>();
        List<String>  flowInputStrings  = flowInput[0].split('\n');

        // Assign inputed work order type
        if (flowInputStrings[0].contains('Unit') && flowInputStrings[1].contains('Clinet')) {
            flowLines.put('Type', 'Fault');
        } else {
            flowLines.put('Type', 'Non-Fault');
        }

        // Setup variables
        String alarmUnit;
        String category; // ?
        Date   dispatched;
        String faultPriority;
        String priority;
        String description;
        String type;
        Boolean reqKeyLock = false;

        // Setup for a FAULT Work Order
        if (flowLines.get('Type') == 'Fault') {
            for (String str : flowInputStrings) {
                if (str.startsWith('Client ID') && str.contains('Unit Number')) {
                    alarmUnit = WorkOrderHelper.returnAlarmUnit(str);
                }
                if (str.startsWith('Clinet Category') && str.contains('Status')) {
                    category = WorkOrderHelper.returnCategoryFault(str);
                }
                if (str.startsWith('Fault Assignment')) {
                    dispatched = WorkOrderHelper.returnDispatchedDate(str);
                }
                if (str.startsWith('Fault Type')) {
                    faultPriority = ' ' + faultPriority + ' ' + str + ' ';
                }
                if (str.startsWith('Priority')) {
                    faultPriority = ' ' + faultPriority + ' ' + str + ' ';
                    priority = WorkOrderHelper.returnFaultPriority(faultPriority);
                    break;
                }
            }      

            // Create description string
            do {
                flowInputStrings.remove(0);
            } while (!flowInputStrings[0].startsWith('Notes History'));

            String workOrderDesc = 'Notes History:';
            for (String str : flowInputStrings) {
                workOrderDesc = workOrderDesc + '\n' + str.removeStart('Notes History:').trim();
            }
            description = workOrderDesc;
            type = 'FAULT';
        } 

        // Setup for a NON-FAULT Work Order
        if (flowLines.get('Type') == 'Non-Fault') {
            for (String str : flowInputStrings) {
                if (str.startsWith('Client ID') && str.contains('Dispatch')) {
                    dispatched = WorkOrderHelper.returnDispatchedDate(str);
                    break;
                }    
            }

            // Create description
            do {
                flowInputStrings.remove(0);
            } while (!flowInputStrings[0].startsWith('Instruction:'));

            String workOrderDesc = 'Work Order Description:';
            for (String str : flowInputStrings) {
                str = str.trim();
                workOrderDesc = workOrderDesc + '\n' + str.removeStart('Instruction: ');
            }
            // Final setup of variables
            description = workOrderDesc;
            priority    = WorkOrderHelper.returnNonFaultPriority(flowInputStrings);
            type        = WorkOrderHelper.returnType(flowInputStrings);
            reqKeyLock  = WorkOrderHelper.keyLockRequired(flowInputStrings);
        }

        // Create new Work Order
        WorkOrder newWorkOrder = new WorkOrder(
            //AccountId = '0011e00000C8btOAAR',
            Status = 'Dispatched',
            Dispatched__c = dispatched,
            priority = priority,
            Work_Order_Description__c = description,
            Type__c = type,
            Key_Lock_Exists__c = reqKeyLock,
            Old_Alarm__c = alarmUnit,
            Pricebook2Id = null
        );
        workOrderResult.add(newWorkOrder);
        return workOrderResult;
    }
}
