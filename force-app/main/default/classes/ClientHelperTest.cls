@IsTest
public class ClientHelperTest {

    // ************************************
    // ***** returnClientId Tests *********
    // ************************************
    @IsTest
    static void clientIdCorrect() {
        
        // Setup
        String faultStr    = 'Client ID 12345 Unit Number MK8917';
        String installStr  = 'Client ID 123456 Dispatch Date 01/01/2000';

        // Act
        String fcidNumeric = ClientHelper.returnClientId(faultStr);
        String icidNumeric = ClientHelper.returnClientId(installStr);
        String fcidCorrect = ClientHelper.returnClientId(faultStr);
        String icidCorrect = ClientHelper.returnClientId(installStr);

        // Assert
        System.assertEquals('12345',  fcidCorrect);
        System.assertEquals('123456', icidCorrect);
    }

    @IsTest
    static void clientIdNumeric() {
        
        // Setup
        String faultStr    = 'Client ID 12345 Unit Number MK8917';
        String installStr  = 'Client ID 123456 Dispatch Date 01/01/2000';

        // Act
        String fcidNumeric = ClientHelper.returnClientId(faultStr);
        String icidNumeric = ClientHelper.returnClientId(installStr);
 
        // Assert
        System.assert(fcidNumeric.isNumeric());
        System.assert(icidNumeric.isNumeric());
    }

    // ************************************
    // ***** returnNames Tests ************
    // ************************************
    @IsTest
    static void oneFirstNameOneLastName() {
        
        // Setup
        String faultStr    = 'Client Name Miss Jenny Citizen Client Contact Name Mr John Citizen';
        String installStr  = 'Client Name Mr John Citizen Phone no. 0300000000 (MOB: 0400000000)';

        // Act
        String fFirstName  = ClientHelper.returnNames(faultStr).get  ('FirstName');
        String fLastName   = ClientHelper.returnNames(faultStr).get  ('LastName' );
        String iFirstName  = ClientHelper.returnNames(installStr).get('FirstName');
        String iLastName   = ClientHelper.returnNames(installStr).get('LastName' );

        // Assert
        System.assertEquals('Jenny',   fFirstName);
        System.assertEquals('Citizen', fLastName );
        System.assertEquals('John',    iFirstName);
        System.assertEquals('Citizen', iLastName );
    }

    @IsTest
    static void multipleFirstNamesMultipleLastNames() {
        
        // Setup
        String faultStr    = 'Client Name Miss Jenny (Jen) Margaret Mc Citizen Client Contact Name Mr John Citizen';
        String installStr  = 'Client Name Mr John (Jack) Van Citizen Phone no. 0300000000 (MOB: 0400000000)';

        // Act
        String fFirstName  = ClientHelper.returnNames(faultStr).get  ('FirstName');
        String fLastName   = ClientHelper.returnNames(faultStr).get  ('LastName' );
        String iFirstName  = ClientHelper.returnNames(installStr).get('FirstName');
        String iLastName   = ClientHelper.returnNames(installStr).get('LastName' );

        // Assert
        System.assertEquals('Jenny (Jen) Margaret', fFirstName);
        System.assertEquals('Mc Citizen',           fLastName );
        System.assertEquals('John (Jack)',          iFirstName);
        System.assertEquals('Van Citizen',          iLastName );
    }

    @IsTest
    static void mulitpleTitles() {
        
        // Setup
        String faultStr    = 'Client Name Mrs & Mr Jenny & John Citizen Client Contact Name Mr John Citizen';
        String installStr  = 'Client Name Mr & Mrs John & Jenny Citizen Phone no. 0300000000 (MOB: 0400000000)';

        // Act
        String fFirstName  = ClientHelper.returnNames(faultStr).get  ('FirstName');
        String fLastName   = ClientHelper.returnNames(faultStr).get  ('LastName' );
        String iFirstName  = ClientHelper.returnNames(installStr).get('FirstName');
        String iLastName   = ClientHelper.returnNames(installStr).get('LastName' );

        // Assert
        System.assertEquals('Jenny & John', fFirstName);
        System.assertEquals('Citizen',      fLastName );
        System.assertEquals('John & Jenny', iFirstName);
        System.assertEquals('Citizen',      iLastName );
    }

    @IsTest
    static void complexNames() {
        
        // Setup
        String faultStr    = 'Client Name Mrs Jenny & John Citizen-Smith Client Contact Name Mr John Citizen';
        String installStr  = 'Client Name Father John & Sister Jenny Di CITIZEN Phone no. 0300000000 (MOB: 0400000000)';

        // Act
        String fFirstName  = ClientHelper.returnNames(faultStr).get  ('FirstName');
        String fLastName   = ClientHelper.returnNames(faultStr).get  ('LastName' );
        String iFirstName  = ClientHelper.returnNames(installStr).get('FirstName');
        String iLastName   = ClientHelper.returnNames(installStr).get('LastName' );

        // Assert
        System.assertEquals('Jenny & John',  fFirstName);
        System.assertEquals('Citizen-Smith', fLastName );
        System.assertEquals('John & Jenny',  iFirstName);
        System.assertEquals('Di Citizen',    iLastName );
    }

    // ************************************
    // ***** returnAddress Tests **********
    // ************************************
    @IsTest
    static void correctAddressOneTownWord() {
        
        // Setup
        String inputStr = 'Client Address 10 Main Street - Local VIC 9999';

        // Act
        String street   = ClientHelper.returnAddress(inputStr).get('Street');
        String city     = ClientHelper.returnAddress(inputStr).get('City');
        String postcode = ClientHelper.returnAddress(inputStr).get('Postcode');

        // Assert
        System.assertEquals('10 Main Street', street);
        System.assertEquals('Local', city);
        System.assertEquals('9999', postcode);
    }

    @IsTest
    static void correctAddressTwoTownWord() {
        
        // Setup
        String inputStr = 'Client Address 10 Main Street - Local South VIC 9999';

        // Act
        String street   = ClientHelper.returnAddress(inputStr).get('Street');
        String city     = ClientHelper.returnAddress(inputStr).get('City');
        String postcode = ClientHelper.returnAddress(inputStr).get('Postcode');

        // Assert
        System.assertEquals('10 Main Street', street);
        System.assertEquals('Local South', city);
        System.assertEquals('9999', postcode);
    }

    @IsTest
    static void complexAddress() {
        
        // Setup
        String inputStr = 'Client Address Unit 1, 199 Main Street (Retirement Village) - LOCAL SOUTH VIC 9999';

        // Act
        String street   = ClientHelper.returnAddress(inputStr).get('Street');
        String city     = ClientHelper.returnAddress(inputStr).get('City');
        String postcode = ClientHelper.returnAddress(inputStr).get('Postcode');

        // Assert
        System.assertEquals('Unit 1, 199 Main Street (Retirement Village)', street);
        System.assertEquals('Local South', city);
        System.assertEquals('9999', postcode);
    }
    // 
    // 

    // ************************************
    // ***** returnPhoneNumbers Tests *****
    // ************************************
    @IsTest
    static void onlyHomePhoneAndNull() {
        
        // Setup
        String faultStr     = 'Client Phone 0300000000 Client Contact Phone 0300000000';
        String installStr   = 'Client Name Mr Test Client Phone no. 0300000000';

        // Act
        String fhomePhone   = ClientHelper.returnPhoneNumbers(faultStr).get  ('HomePhone'  );
        String fmobilePhone = ClientHelper.returnPhoneNumbers(faultStr).get  ('MobilePhone');
        String ihomePhone   = ClientHelper.returnPhoneNumbers(installStr).get('HomePhone'  );
        String imobilePhone = ClientHelper.returnPhoneNumbers(installStr).get('MobilePhone');

        // Assert
        System.assertEquals('0300000000', fhomePhone  );
        System.assertEquals(null,         fmobilePhone);
        System.assertEquals('0300000000', ihomePhone  );
        System.assertEquals(null,         imobilePhone);
    }

    @IsTest
    static void onlyMobilePhoneAndNull() {
        
        // Setup
        String faultStr     = 'Client Phone (MOB: 0400000000) Client Contact Phone 0300000000';
        String installStr   = 'Client Name Mr Test Client Phone no. (MOB: 0400000000)';

        // Act
        String fhomePhone   = ClientHelper.returnPhoneNumbers(faultStr).get  ('HomePhone'  );
        String fmobilePhone = ClientHelper.returnPhoneNumbers(faultStr).get  ('MobilePhone');
        String ihomePhone   = ClientHelper.returnPhoneNumbers(installStr).get('HomePhone'  );
        String imobilePhone = ClientHelper.returnPhoneNumbers(installStr).get('MobilePhone');

        // Assert
        System.assertEquals(null,         fhomePhone  );
        System.assertEquals('0400000000', fmobilePhone);
        System.assertEquals(null,         ihomePhone  );
        System.assertEquals('0400000000', imobilePhone);
    }

    @IsTest
    static void bothPhoneNumbers() {
        
        // Setup
        String faultStr     = 'Client Phone 0300000000 (MOB: 0400000000) Client Contact Phone 0311111111';
        String installStr   = 'Client Name Mr Test Client Phone no. 0300000000 (MOB: 0400000000)';

        // Act
        String fhomePhone   = ClientHelper.returnPhoneNumbers(faultStr).get  ('HomePhone'  );
        String fmobilePhone = ClientHelper.returnPhoneNumbers(faultStr).get  ('MobilePhone');
        String ihomePhone   = ClientHelper.returnPhoneNumbers(installStr).get('HomePhone'  );
        String imobilePhone = ClientHelper.returnPhoneNumbers(installStr).get('MobilePhone');

        // Assert
        System.assertEquals('0300000000', fhomePhone  );
        System.assertEquals('0400000000', fmobilePhone);
        System.assertEquals('0300000000', ihomePhone  );
        System.assertEquals('0400000000', imobilePhone);
    }

    @IsTest
    static void noPhoneNumbers() {
        
        // Setup
        String faultStr     = 'Client Phone No LL (MOB: No LL) Client Contact Phone No LL';
        String installStr   = 'Client Name Mr Test Client Phone no. No LL (MOB: No LL)';

        // Act
        String fhomePhone   = ClientHelper.returnPhoneNumbers(faultStr).get  ('HomePhone'  );
        String fmobilePhone = ClientHelper.returnPhoneNumbers(faultStr).get  ('MobilePhone');
        String ihomePhone   = ClientHelper.returnPhoneNumbers(installStr).get('HomePhone'  );
        String imobilePhone = ClientHelper.returnPhoneNumbers(installStr).get('MobilePhone');

        // Assert
        System.assertEquals(null, fhomePhone  );
        System.assertEquals(null, fmobilePhone);
        System.assertEquals(null, ihomePhone  );
        System.assertEquals(null, imobilePhone);
    }

    @IsTest
    static void incorrectHomePhoneOne() {
        
        // Setup
        String faultStr     = 'Client Phone 00000000 Client Contact Phone 0311111111';
        String installStr   = 'Client Name Mr Test Client Phone no. 00000000';

        // Act
        String fhomePhone   = ClientHelper.returnPhoneNumbers(faultStr).get  ('HomePhone'  );
        String fmobilePhone = ClientHelper.returnPhoneNumbers(faultStr).get  ('MobilePhone');
        String ihomePhone   = ClientHelper.returnPhoneNumbers(installStr).get('HomePhone'  );
        String imobilePhone = ClientHelper.returnPhoneNumbers(installStr).get('MobilePhone');

        // Assert
        System.assertEquals('0300000000', fhomePhone  );
        System.assertEquals(null,         fmobilePhone);
        System.assertEquals('0300000000', ihomePhone  );
        System.assertEquals(null,         imobilePhone);
    }

    @IsTest
    static void addFirstMobileNotSecond() {
        
        // Setup
        String faultStr     = 'Client Phone 0400000000 (MOB: 0411111111) Client Contact Phone 0311111111';
        String installStr   = 'Client Name Mr Test Client Phone no. 0400000000 (MOB: 0411111111)';

        // Act
        String fhomePhone   = ClientHelper.returnPhoneNumbers(faultStr).get  ('HomePhone'  );
        String fmobilePhone = ClientHelper.returnPhoneNumbers(faultStr).get  ('MobilePhone');
        String ihomePhone   = ClientHelper.returnPhoneNumbers(installStr).get('HomePhone'  );
        String imobilePhone = ClientHelper.returnPhoneNumbers(installStr).get('MobilePhone');

        // Assert
        System.assertEquals(null,         fhomePhone  );
        System.assertEquals('0400000000', fmobilePhone);
        System.assertEquals(null,         ihomePhone  );
        System.assertEquals('0400000000', imobilePhone);
    }

    @IsTest
    static void incorrectMobilePhoneOne() {
        
        // Setup
        String faultStr     = 'Client Phone 0300000000 (MOB: 0400000000 ) Client Contact Phone 0311111111';
        String installStr   = 'Client Name Mr Test Client Phone no. 0300000000 (MOB: 0400000000 )';

        // Act
        String fhomePhone   = ClientHelper.returnPhoneNumbers(faultStr).get  ('HomePhone'  );
        String fmobilePhone = ClientHelper.returnPhoneNumbers(faultStr).get  ('MobilePhone');
        String ihomePhone   = ClientHelper.returnPhoneNumbers(installStr).get('HomePhone'  );
        String imobilePhone = ClientHelper.returnPhoneNumbers(installStr).get('MobilePhone');

        // Assert
        System.assertEquals('0300000000', fhomePhone  );
        System.assertEquals('0400000000', fmobilePhone);
        System.assertEquals('0300000000', ihomePhone  );
        System.assertEquals('0400000000', imobilePhone);
    }

    @IsTest
    static void incorrectMobilePhoneTwo() {
        
        // Setup
        String faultStr     = 'Client Phone 0300000000 (MOB: 0400 000 000 ) Client Contact Phone 0311111111';
        String installStr   = 'Client Name Mr Test Client Phone no. 0300000000 (MOB: 0400 000 000 )';

        // Act
        String fhomePhone   = ClientHelper.returnPhoneNumbers(faultStr).get  ('HomePhone'  );
        String fmobilePhone = ClientHelper.returnPhoneNumbers(faultStr).get  ('MobilePhone');
        String ihomePhone   = ClientHelper.returnPhoneNumbers(installStr).get('HomePhone'  );
        String imobilePhone = ClientHelper.returnPhoneNumbers(installStr).get('MobilePhone');

        // Assert
        System.assertEquals('0300000000', fhomePhone  );
        System.assertEquals(null,         fmobilePhone);
        System.assertEquals('0300000000', ihomePhone  );
        System.assertEquals(null,         imobilePhone);
    }    

    // ************************************
    // ***** returnLocationRef Tests ******
    // ************************************
    @IsTest
    static void locationRefCorrect() {
        
        // Setup
        String faultStr = 'Map Ref. Main South-East Heights Rd';

        // Act
        String result   = ClientHelper.returnLocationRef(faultStr);

        // Assert
        System.assertEquals('Main South-East Heights Rd', result);
    }  
}
