public with sharing class ClientHelper {

    public static String returnClientId(String inputStr) {
        if (inputStr == null) {
            return null;
        } 
        inputStr = inputStr.trim();
        inputStr = inputStr.removeStart('Client ID ');
        List<String> inputStrList = inputStr.split(' ');
        return inputStrList[0];
    }

    // Map Keys: 'FirstName' 'LastName'
    public static Map<String, String> returnNames(String inputStr) {
        //Setup main two variables
        inputStr = inputStr.trim();
        inputStr = inputStr.removeStart('Client Name ');
        List<String> inputStrList = inputStr.split(' ');
        // Set up other variables
        Map<String, String> results = new Map<String, String>();
        List<String> titles         = new List<String>{'Mr', 'Mrs', 'Miss', 'Ms', '&', 'Father', 'Sister', 'Other'};
        List<String> lnamePrefix    = new List<String>{'Mc', 'Mac', 'D\'', 'O\'', 'Van', 'Dal', 'De', 'Di'};
        List<String> breakStr       = new List<String>{'Phone', 'Client'};
        List<String> firstNames     = new List<String>();
        List<String> lastNames      = new List<String>();
        List<String> lastNamesFixed = new List<String>();
        Integer ampersandFound      = 0;

        for (String item : inputStrList) {
            if (item == '&' && ampersandFound == 0) {
                ampersandFound++;
                if (firstNames.size() > 0) {
                    firstNames.add(item);
                }
                continue;
            } 
            if (item == '&' && ampersandFound == 1) {
                firstNames.add(item);
                continue;
            }
            if (lnamePrefix.contains(item)) {
                if (firstNames.size() > 0) {
                    lastNames.add(item);
                }
                continue;
            }
            if (breakStr.contains(item)) {
                break;
            }
            if (!titles.contains(item)) {
                firstNames.add(item);
            }
        }
        // Finalise strings for map
        Integer lnamePos = firstNames.size() - 1;
        lastNames.add(firstNames.remove(lnamePos));

        // Correct capatalised last names
        for (String lname : lastNames) {
            if (!lname.contains('-')) {
                lname = lname.toLowerCase();
                lname = lname.capitalize();
            }
            lastNamesFixed.add(lname);
        }

        results.put('FirstName', String.join(firstNames, ' '));
        results.put('LastName',  String.join(lastNamesFixed, ' '));
        return results;
    }

    // Map Keys: 'Street' 'City' 'Postcode'
    public static Map<String, String> returnAddress(String inputStr) {
        inputStr = inputStr.trim();
        inputStr = inputStr.removeStart('Client Address ');

        Map<String, String> results = new Map<String, String>();
        List<String> streetStrList  = new List<String>();
        List<String> cityStrList    = new List<String>();
        List<String> inputStrList = inputStr.split(' ');

        do { // Street details
            streetStrList.add(inputStrList[0]);
            inputStrList.remove(0);
        }   while (inputStrList[0] != '-');

        if (inputStrList[0] == '-') {
            inputStrList.remove(0);
        }

        do { // City details
            inputStrList[0] = inputStrList[0].toLowerCase();
            inputStrList[0] = inputStrList[0].capitalize();
            cityStrList.add(inputStrList[0]);
            inputStrList.remove(0);
        }   while (inputStrList[0] != 'VIC');

        if (inputStrList[0] == 'VIC') {
            inputStrList.remove(0);
        }

        results.put('Street', String.join(streetStrList, ' '));
        results.put('City', String.join(cityStrList, ' '));
        results.put('Postcode', inputStrList[0]);
        return results;
    }

    // Map Keys: 'HomePhone' 'MobilePhone'
    public static Map<String, String> returnPhoneNumbers(String inputStr) {
        inputStr = inputStr.trim();
        Map<String, String> results = new Map<String, String>();
        List<String> inputStrList = inputStr.split(' ');
        String homePhoneKey         = 'HomePhone';
        String mobilePhoneKey       = 'MobilePhone';

        for (String item : inputStrList) {

            if (item.isNumeric() && item.startsWith('03') && item.length() == 10) {
                if (!results.containsKey(homePhoneKey)) {
                    results.put(homePhoneKey, item);
                    System.debug('item===' + item);   
                    System.debug('results===' + results);  
                    continue; 
                }
            }
            if (item.isNumeric() && item.length() == 8) {
                if (!results.containsKey(homePhoneKey)) {
                    item = '03' + item;
                    results.put(homePhoneKey, item);
                    System.debug('item===' + item);   
                    System.debug('results===' + results);  
                    continue;                    
                }
            }
            if (item.startsWith('04') && item.endsWith(')') && item.length() == 11) {
                if (!results.containsKey(mobilePhoneKey)) {
                    item = item.removeEnd(')');
                    results.put(mobilePhoneKey, item);
                    break;                    
                }
            }
            if (item.startsWith('04') && item.length() == 10) {
                if (!results.containsKey(mobilePhoneKey)) {
                    results.put(mobilePhoneKey, item);
                    break;                    
                }
            }
        }
        return results;
    }

    public static String returnLocationRef(String inputStr) {
        if (inputStr == null) {
            return null;
        }
        inputStr = inputStr.trim();
        inputStr = inputStr.removeStart('Map Ref. ');
        return inputStr;
    }
}
