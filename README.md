# Work Order Creator App

**Hi Ivan,**

Adam asked me what piece of functionality I have created in my org that i'm most proud of, and this is it.

The app is nothing flash, the functionality is the Apex code that picks out all the pieces of the text file and creates Contacts, Work Orders, Work Order Line Items, Assets, and adds prices from Pricebooks to the Work Order Line Item.

This app is a trimmed up version of what I use in my production org for work, I thought it would be overkill to add all the Asset and Pricebook details when I just want to show you I can write some Apex and Flows.

After you clone, create a scratch org, and push the metadata you need to run this script:

    sfdx force:apex:execute -f ./scripts/apex/assignPermissionSet.apex

Then navigate to Work Order Creator App.

## Explore

I get two types of text files sent to me for jobs. **Installs:**

    Client ID 123456 Dispatch Date 15/01/2021
    Client Name Mr John Citizen Phone no. 0300000000 (MOB: 0400000000)
    Client Address 10 Main Street - Some Town VIC 3000
    Instruction: DHS client doing dcs
    Also install a keylock

and **Faults:**

    Client ID 654321 Unit Number MA1000
    Clinet Category DHS Client Status ONLINE
    Client Name Mrs Joanne Citizen Client Contact Name Mrs Jenny Citizen
    Client Phone 0300000000 (MOB: 0400000000) Client Contact Phone 0311111111
    Client Contact Mobile (MOB: 0411111111)
    Contact Relationship Friend/ neighbour - female
    Client Address 50 South Street - Other Town VIC 3000
    Map Ref. East West Lane
    Fault Assignment Date 15/01/2021
    Fault Type VISIT
    Priority High
    Notes History: <15/01/2021 17:00:00 AM Help Desk>
    EMAIL TO BE SENT TO THE TECHNICIAN.
    <15/01/2021 16:00:00 AM Client Services>
    Client phoned back to advise of an issue.

Once you open the scratch org, the flow is located in the bottom left Utility Bar. Here is what is happening as you go through the screens.


![01](resources/01.jpg)

Paste the job, Quick Add will skip the next three screens. I have it on by default.



![02](resources/02.jpg) 

It's already extracted the Client ID and Date, now it's showing you the contacts name incase you wish to modify.
**Note:** if you add the same job again, the flow will now also show you the details already stored in Salesforce.  Now you can update or keep the data as needed.



![03](resources/03.jpg)

Now the flow has found all relevant address data and puts it up for display.



![04](resources/04.jpg)

Same again for phone information.



![05](resources/05.jpg)

Now here I input a reference like `Cnr Some Street & Some Other Street`, and the distance in km between my work and the contacts address.  If I was really fancy I would have built an integration to automatically get this information but haven't quite got there yet!



![06](resources/06.jpg)

Comparison between the inputed data, and the original text file data.



![07](resources/07.jpg)

Now we're about to create the Work Order.  `Type` is a custom picklist to reference the type of job i've been sent. `Priority` is a modified standard picklist is use as a sub-type.  Both of these have been automatically set by the Apex code finding certain words in the job text. `Date` and `Description` are created from the job text. `Keylock` checkbox is `true` because the code found keywords in the job text.



![08](resources/08.jpg)

Complete.

This works the same if you do another using the fault text as an input. You can change any data in text, but not the text's structure.  If the code does not find certain words in the correct place it does not like it!

I hope these two repo's give you an idea of where i'm up to so far...